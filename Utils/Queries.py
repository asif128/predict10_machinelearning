from Utils.LoadConfig import GetDBConfig
import pyodbc
import pandas as pd
from datetime import *

from datetime import datetime
from Utils.colors import *

def ConenctToDB():
    server, database, username, password = GetDBConfig()
    cnxn = pyodbc.connect(
        'DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    cursor = cnxn.cursor()
    return (cnxn, cursor)

def GetDataFromDB_PerStock(stock):
    cnxn, cursor = ConenctToDB()
    query="""
    DECLARE @stock_name NVARCHAR(128) ='"""+stock+"""'
    DECLARE @ml_algo_offset DATETIME = (SELECT ml_algo_offset FROM dbo.stocks WHERE name = @stock_name)
    SELECT distinct [s_dt]
          ,[s_open]
          ,[s_high]
          ,[s_low]
          ,[s_close]
          ,[s_volume]
      FROM [predict10].[dbo].[stocks_data]
      WHERE s_name= @stock_name AND (s_dt>@ml_algo_offset OR @ml_algo_offset IS NULL)"""
    results = cursor.execute(query)
    stocks = list()

    for row in results:
        stocks.append(row)

    return stocks


def GetDataFromDB_PerStock_TODF(stock):
    cnxn, cursor = ConenctToDB()
    query="""
    DECLARE @stock_name NVARCHAR(128) ='"""+stock+"""'
    DECLARE @ml_algo_offset DATETIME = (SELECT ml_algo_offset FROM dbo.stocks WHERE name = @stock_name)
    SELECT [s_dt]
          ,[s_open]
          ,[s_high]
          ,[s_low]
          ,[s_close]
          ,[s_volume]
      FROM [predict10].[dbo].[stocks_data]
      WHERE s_name= @stock_name AND (s_dt>@ml_algo_offset OR @ml_algo_offset IS NULL)"""
    result_port_map = pd.read_sql(query, cnxn)

    return result_port_map


def GetStocksList():
    cnxn, cursor = ConenctToDB()
    results = cursor.execute("SELECT name FROM dbo.stocks")
    stocks = list()


    for  row in results:
        stocks.append(row[0])

    return stocks

def GetMaxPredictSycle_PerStock(stock):
    cnxn, cursor = ConenctToDB()
    query = """
    SELECT ISNULL(x.max_p_predict_cycle,0) AS [max_p_predict_cycle]
  FROM [predict10].[dbo].[stocks] AS s
  LEFT JOIN (SELECT p_name,MAX([p_predict_cycle]) AS [max_p_predict_cycle] FROM [dbo].[predcits_data]
  GROUP BY p_name ) AS x
  ON x.p_name = s.name
  WHERE name ='"""+stock+"""'"""

    ls = list()

    results = cursor.execute(query)

    for row in results:
        ls.append(row[0])
    time = ls[0]

    return time


def UpdateStockOffset(stock, datetime):
    cnxn, cursor = ConenctToDB()
    results = cursor.execute("UPDATE [dbo].[stocks] SET [ml_algo_offset]=? WHERE name =?", datetime, stock)

    cnxn.commit()
    cursor.close()


def InsertPredictedData(predict_data, stock, run_sycle):
    # Insert Dataframe into SQL Server:
    cnxn, cursor = ConenctToDB()
    for timestamp, price in predict_data:
        date = datetime.fromtimestamp(int(timestamp))- timedelta(hours=3)
        cursor.execute(
            "INSERT INTO [dbo].[predcits_data]([p_name],[p_dt],[p_close],[p_predict_cycle])VALUES (?,?,?,?)", stock, date, price, run_sycle)
    cnxn.commit()
    cursor.close()


