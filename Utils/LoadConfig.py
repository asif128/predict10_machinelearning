import json

def GetDBConfig():
    with open('Utils/config.json') as config_file:
        data = json.load(config_file)

    db_conf=data['DB config']
    Server = db_conf['Server']
    Database = db_conf['Database']
    User = db_conf['User']
    Pass = db_conf['Pass']
    return (Server, Database, User, Pass)