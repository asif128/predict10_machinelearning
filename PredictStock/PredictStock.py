from Utils.Queries import *
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from keras.layers import Dense, Dropout, LSTM


import numpy as np
import datetime, pytz
from datetime import *
import time


def PredictStock(stock):

    #Update stock offset to null
    UpdateStockOffset(stock, None)

    # Build model
    model_arr = []
    model_arr.append(LinearRegression())
   # model_arr.append(SVR(kernel='rbf', C=1e3, gamma=0.1))
    model_arr.append(LSTM(units=50))

    while (1 == 1 ):
        now = datetime.now()

        try:
            # Get new Data
            df = GetDataFromDB_PerStock_TODF(stock)
            if(df.empty == False):

            # max_df_date
                max_df_date=max(df["s_dt"].tolist())

            # Get new Data
                predict_sycle = GetMaxPredictSycle_PerStock(stock) + 1

            # Train model
                for model in model_arr :
                    model = TrainModle(model, df)

            # Update DB offset
                UpdateStockOffset(stock, max_df_date)

            # predict Data
                for model in model_arr:
                    predict_data = PredictModle_Next_60min(model)

            #Insert Into DB
                    InsertPredictedData(predict_data, stock, predict_sycle)


            # save predicted data to DB + with predictSycle++

        except Exception as e:
            print(bcolors.FAIL + 'Predict Data Failed!!!' + bcolors.ENDC)
            print('--------------------')
            print(bcolors.FAIL + str(e) + bcolors.ENDC)

        while (datetime.now() < now + timedelta(minutes=1)):
            time.sleep(1)


def TrainModle(ml_modle,df):
    prices = df[df.columns[0:2]]

    prices["timestamp"] = (prices.s_dt - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')
    prices = prices.drop(['s_dt'], axis=1)

    dataset = prices.values
    X = dataset[:, 1].reshape(-1, 1)
    Y = dataset[:, 0:1]


    X = X.tolist()
    X = np.reshape(X, (len(X), 1))
    Y = Y.tolist()

    ml_modle.fit(X, Y)

    return ml_modle

def PredictModle_Next_60min(ml_modle):
    predict_timestamp_list = []
    nyc_datetime = datetime.now(pytz.timezone('US/Eastern'))
    for i in range(1, 61):
        predict_timestamp_list.append((nyc_datetime + timedelta(minutes=i)- timedelta(hours=4)).timestamp())
    predict_timestamp_list = np.array(predict_timestamp_list).reshape(-1, 1)
    predict_datetime_list = np.asarray(list(map(lambda x: datetime.fromtimestamp(int(x)),predict_timestamp_list)))

    # Print linear regression model predictions for the next '30' days
    ml_modle_prediction = ml_modle.predict(predict_timestamp_list)
    print(ml_modle_prediction)
    predict_data = np.concatenate((predict_timestamp_list, ml_modle_prediction), axis=1)

    return predict_data