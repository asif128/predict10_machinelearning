from Utils.Queries import *
from sklearn.linear_model import Lasso
from sklearn.linear_model import ElasticNet
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import datetime, pytz
import numpy as np
from datetime import *


stock='tsla'
df = GetDataFromDB_PerStock_TODF(stock)

prices = df[df.columns[0:2]]
#prices.reset_index(level=0, inplace=True)
#prices["timestamp"] = pd.to_datetime(prices.date).astype(int) // (10**9)
prices["timestamp"]=(prices.s_dt - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')
prices = prices.drop(['s_dt'], axis=1)

dataset = prices.values
X = dataset[:,1].reshape(-1,1)
Y = dataset[:,0:1]

X = X.tolist()
X=np.reshape(X,(len(X), 1))
Y = Y.tolist()


# Create and train the Support Vector Machine (Regressor)
svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)
#svr_rbf.fit(X, Y)


# Create and train the Linear Regression  Model
lr = LinearRegression()
# Train the model
lr.fit(X, Y)

predict_timestamp_list = []
nyc_datetime = datetime.now(pytz.timezone('US/Eastern')) - timedelta(hours=4)
for i in range(1, 61):
    predict_timestamp_list.append((nyc_datetime+ timedelta(minutes=i)).timestamp())
predict_timestamp_list = np.array(predict_timestamp_list).reshape(-1,1)
# predict_datetime_list = np.asarray(list(map(lambda x: datetime.fromtimestamp(int(x)),predict_timestamp_list)))


# Print linear regression model predictions for the next '30' days
lr_prediction = lr.predict(predict_timestamp_list)
print(lr_prediction)
predict_data=np.concatenate((predict_datetime_list,lr_prediction),axis=1)
# Print support vector regressor model predictions for the next '30' days
svm_prediction = svr_rbf.predict(predict_timestamp_list)
print(svm_prediction)