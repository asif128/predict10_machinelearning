from Utils.Queries import *
from sklearn.linear_model import Lasso
from sklearn.linear_model import ElasticNet
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

import numpy as np

stock='tsla'
df = GetDataFromDB_PerStock_TODF(stock)

prices = df[df.columns[0:2]]
#prices.reset_index(level=0, inplace=True)
#prices["timestamp"] = pd.to_datetime(prices.date).astype(int) // (10**9)
prices["timestamp"]=(prices.s_dt - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')
prices = prices.drop(['s_dt'], axis=1)

dataset = prices.values
X = dataset[:,1].reshape(-1,1)
Y = dataset[:,0:1]

validation_size = 0.15
seed = 7

#X_train, X_validation, Y_train, Y_validation = train_test_split(X, Y, test_size=validation_size, random_state=seed)



# Test options and evaluation metric
num_folds = 10
seed = 7
scoring = "r2"

# Spot-Check Algorithms
models = []

models.append((' SVR ', SVR()))



# evaluate each model in turn
results = []
names = []
for name, model in models:
    kfold = KFold(n_splits=num_folds, random_state=seed, shuffle=True)
    cv_results = cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
    # print(cv_results)
    results.append(cv_results)
    names.append(name)
    msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
    print(msg)

    # Future prediction, add dates here for which you want to predict
dates = ["2021-05-27", "2021-05-28", "2021-05-29", "2021-05-30",]
# convert to time stamp
for dt in dates:
    datetime_object = datetime.strptime(dt, "%Y-%m-%d")
    timestamp = datetime.timestamp(datetime_object)
    # to array X
    np.append(X, int(timestamp))



# Define model
model = DecisionTreeRegressor()
# Fit to model
model.fit(X_train, Y_train)
# predict
predictions = model.predict(Xp)
print(mean_squared_error(Y, predictions))

# %matplotlib inline
fig = plt.figure(figsize=(24, 12))
plt.plot(X, Y)
plt.plot(X, predictions)
plt.show()